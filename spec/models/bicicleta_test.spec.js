var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');


describe('Testing Bicicletas', function () {
    beforeEach(function (done) {
							  
        mongoose.disconnect();
        mongoose.connect('mongodb://localhost/bicicletas', {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });


        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Error de conexion'));
        db.once('open', function () {
            console.log('Conectados la base de datos');

            done();
        });

    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);

            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "montaña", [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("montaña");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        })
    });

    describe('Bicicleta.allBicicletas', () => {
        it('Comienza vacia', (done) => {
            Bicicleta.allBicicletas(function (err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });


    describe('Bicicleta.add', () => {
        it('Agresa solo una bici', (done) => {
            var abici = new Bicicleta({ code: 1, color: "verde", modelo: "montaña" });
            Bicicleta.add(abici, function (err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicicletas(function (err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(abici.code);

                    done();
                })
            })
        })
    })

				   
			   
		   
	   

    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bicicleta con code 1', (done) => {
            Bicicleta.allBicicletas(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici1 = new Bicicleta({ code: 1, color: "verde", modelo: "montaña" });
                Bicicleta.add(aBici1, function (err, newBici) {
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({ code: 2, color: "azul", modelo: "pistera" });
                    Bicicleta.add(aBici2, function (err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function (err, targetBici) {
                            expect(targetBici.code).toBe(aBici1.code);
                            expect(targetBici.color).toBe(aBici1.color);
                            expect(targetBici.modelo).toBe(aBici1.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });

});




						  
								
	 

											
									  
							   
												  
	   

   

							   
							  
												 

																			
						

												 
											 

	   
   

									  
												
												  
														
														  
							 
							  

											   
									  
												   
													 

	   
	  